RSpec.describe GameController do
  let(:game_controller) { GameController.new }
  let(:game) { game_controller.game }

  describe 'initialize' do
    subject { game_controller }

    it do
      expect(game).to be_an_instance_of RobotToyJonhnes::Game
    end
  end

  describe 'run' do
    let(:command) { '2' }

    after { game_controller.run }

    it do
      expect(game_controller).to receive(:trap).with('SIGINT')
      expect(game_controller).to receive(:system).with('clear') { nil }
      expect(game_controller).to receive(:puts).at_most(3).times
      expect(game_controller).to receive(:gets) { '2' }
      expect(game_controller).to receive(:loop).and_yield
    end
  end

  describe 'decode_command' do
    before do
      allow(game_controller).to receive_message_chain(:gets, :chomp) { 'command' }
      allow(game_controller).to receive(:puts) { nil }
    end
    after { game_controller.decode_command command }

    context 'when PLACE command' do
      let(:command) { '1' }

      it do
        expect(game).to receive(:place).and_return(nil)
      end
    end

    context 'when MOVE command' do
      let(:command) { '2' }

      it do
        expect(game).to receive(:move).and_return(nil)
      end
    end

    context 'when LEFT command' do
      let(:command) { '3' }

      it do
        expect(game).to receive(:rotate_left).and_return(nil)
      end
    end

    context 'when RIGHT command' do
      let(:command) { '4' }

      it do
        expect(game).to receive(:rotate_right).and_return(nil)
      end
    end

    context 'when REPORT command' do
      let(:command) { '5' }

      it do
        expect(game).to receive(:report).and_return(nil)
      end
    end

    context 'when EXIT command' do
      let(:command) { '6' }

      it do
        expect(game_controller).to receive(:puts).with('Bye').and_return(nil)
        expect(game_controller).to receive(:exit).and_return(nil)
      end
    end

    context 'when is unknown command' do
      let(:command) { 'test' }

      it do
        expect(game_controller).to receive(:puts) { nil }
        expect(game_controller).to receive_message_chain(:gets, :chomp) { nil }
      end
    end
  end
end