# frozen_string_literal: true

Dir['./app/*.rb'].sort.each { |file| require file }
require 'robot_toy_jonhnes'

GameController.new.run