# frozen_string_literal: true

# Control game cli interface
class GameController
  attr :game

  def initialize
    @game = RobotToyJonhnes::Game.new
  end

  def run
    trap 'SIGINT' do
      puts "\nBye"
      exit
    end
    loop do
      command_header
      command = gets.chomp
      decode_command command
    end
  end

  def command_header
    system 'clear'
    puts 'Choose a command'
    puts 'PLACE(1) MOVE(2) LEFT(3) RIGHT(4) REPORT(5) EXIT(6)'
  end

  def decode_command(command)
    command_options = {
      '1' => :place_command, '2' => :move_command, '3' => :rotate_left_command,
      '4' => :rotate_right_command, '5' => :report_command, '6' => :exit_command
    }

    send(command_options[command])
  rescue StandardError => e
    puts e
    gets.chomp
  end

  def new_place_from_user
    puts 'Digit the position and facing'
    gets.chomp
  end

  def place_command
    new_place = new_place_from_user
    @game.place new_place
  end

  def move_command
    @game.move
  end

  def rotate_left_command
    @game.rotate_left
  end

  def rotate_right_command
    @game.rotate_right
  end

  def report_command
    puts @game.report
    gets.chomp
  end

  def exit_command
    puts 'Bye'
    exit
  end
end